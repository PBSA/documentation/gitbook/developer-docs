# 1.0.0

The below sections help in understanding the ways of deployment and development.

{% content-ref url="infrastructure/" %}
[infrastructure](infrastructure/)
{% endcontent-ref %}

{% content-ref url="development/" %}
[development](development/)
{% endcontent-ref %}
