# Deployment to AWS ECS





{% content-ref url="building-the-docker-images.md" %}
[building-the-docker-images.md](building-the-docker-images.md)
{% endcontent-ref %}

{% content-ref url="storing-secrets-in-amazon-parameter-store-to-use-in-ecs.md" %}
[storing-secrets-in-amazon-parameter-store-to-use-in-ecs.md](storing-secrets-in-amazon-parameter-store-to-use-in-ecs.md)
{% endcontent-ref %}

{% content-ref url="creating-the-task-definition.md" %}
[creating-the-task-definition.md](creating-the-task-definition.md)
{% endcontent-ref %}

{% content-ref url="creating-the-cluster.md" %}
[creating-the-cluster.md](creating-the-cluster.md)
{% endcontent-ref %}

{% content-ref url="creating-the-service.md" %}
[creating-the-service.md](creating-the-service.md)
{% endcontent-ref %}
