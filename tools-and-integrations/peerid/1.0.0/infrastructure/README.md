# Infrastructure

The two ways of deployment are explained in the documents below:

{% content-ref url="deployment-on-a-linux-serve.md" %}
[deployment-on-a-linux-serve.md](deployment-on-a-linux-serve.md)
{% endcontent-ref %}

{% content-ref url="deployment-to-aws-ecs/" %}
[deployment-to-aws-ecs](deployment-to-aws-ecs/)
{% endcontent-ref %}
