# Development

The following documents help to understand the requirements and authentication process of PeerID:

{% content-ref url="how-does-peerid-work-without-storing-the-keys.md" %}
[how-does-peerid-work-without-storing-the-keys.md](how-does-peerid-work-without-storing-the-keys.md)
{% endcontent-ref %}

{% content-ref url="authentication-with-peerid.md" %}
[authentication-with-peerid.md](authentication-with-peerid.md)
{% endcontent-ref %}

{% content-ref url="brain-storming.md" %}
[brain-storming.md](brain-storming.md)
{% endcontent-ref %}

{% content-ref url="software-requirements.md" %}
[software-requirements.md](software-requirements.md)
{% endcontent-ref %}
