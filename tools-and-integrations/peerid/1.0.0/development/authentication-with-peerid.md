# Authentication with PeerID

&#x20;`{`\
&#x20;   `"result": {`\
&#x20;       `"expires": "<expiry date of the token>",`\
&#x20;       `"app_id": <your client ID>,`\
&#x20;       `"scope": <permissions granted by the user>,`\
&#x20;       `"token": "<access token for the user>",`\
&#x20;       `"refresh_token": "<refresh token for the user>"`\
&#x20;   `},`\
&#x20;   `"status": 200`\
`}`

Note: Store the access token and refresh token like passwords.

**OAuth Resource Owner Password Credentials flow**\
Follow the below steps to get the access token using the OAuth Resource Owner Password Credentials flow:

1.  In your app, request for user's login credentials i.e. username or email ID and mobile or password and on your server, get an access token by making this request:\


    `POST https://peerid.peerplays.download/api/v1/auth/token`\
    &#x20;    `?login=<user's username or email ID>`\
    &#x20;    `&password=<user's password>`\
    &#x20;    `&mobile=<user's mobile number>`\
    &#x20;    `&client_id=<your client ID>`

    \
    Either `password` or `mobile` parameters has to be passed in this request. If both are passed, the PeerID server validates both of them along with the `login`.\

2.  We respond with a json-encoded access token. The response looks like this:

    &#x20;`{`\
    &#x20;   `"result": {`\
    &#x20;       `"expires": "<expiry date of the token>",`\
    &#x20;       `"app_id": <your client ID>,`\
    &#x20;       `"scope": <permissions granted by the user>,`\
    &#x20;       `"token": "<access token for the user>",`\
    &#x20;       `"refresh_token": "<refresh token for the user>"`\
    &#x20;   `},`\
    &#x20;   `"status": 200`\
    `}`

## **Sending access tokens**

Once you have the user’s access token, your app can perform the permitted operations on behalf of the user on the Peerplays blockchain using the `/api/v1/app/operations` API. You have to pass the access token for the user in the Authorization header for this API like:

`curl -H "Authorization: Bearer <access token>" https://peerid.peerplays.download/api/v1/app/operations`

Note: The access token for one app cannot be used for another app.

## **Refreshing access token**

New OAuth2 access tokens have expirations. Token-expiration periods vary in length, based on how and when the token was acquired. Tokens return an `expires` field indicating how long the token should last. However, you should build your applications in such a way that they are resilient to token authentication failures. In other words, an application capable of refreshing tokens should not need to know how long a token will live. Rather, it should be prepared to deal with the token becoming invalid at any time.

On seeing a `401 - Unauthorized` error, an application should try to refresh the session if a refresh token is present. If the refresh fails, the application should re-prompt the end user with another authentication dialog via the standard OAuth 2 flow.

Generally, refresh tokens are used to extend the lifetime of a given authorization.

**How to refresh:**

To refresh a token, you need the refresh\_token that you get in the response when you exchange your code for the token and the client ID and client secret. The following API returns the new access token:

`POST https://peerid.peerplays.download/api/v1/auth/refreshtoken`\
&#x20;    `?refresh_token=<refresh token for the user>`\
&#x20;    `&client_id=<your client ID>`\
&#x20;    `&client_secret=<your client secret>`

The response will look like this:

`{`\
&#x20;   `"result": {`\
&#x20;       `"expires": "<expiry date of the token>",`\
&#x20;       `"app_id": <your client ID>,`\
&#x20;       `"scope": <permissions granted by the user>,`\
&#x20;       `"token": "<access token for the user>",`\
&#x20;       `"refresh_token": "<refresh token for the user>"`\
&#x20;   `},`\
&#x20;   `"status": 200`\
`}`
