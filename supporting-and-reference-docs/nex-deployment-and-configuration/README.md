---
description: The step-by-step procedure to install NEX on-chain
---

# NEX Deployment & Configuration

{% content-ref url="nex-deployment.md" %}
[nex-deployment.md](nex-deployment.md)
{% endcontent-ref %}

{% content-ref url="nex-blockchain-api-configuration.md" %}
[nex-blockchain-api-configuration.md](nex-blockchain-api-configuration.md)
{% endcontent-ref %}

{% content-ref url="deploying-nex-in-a-ha-scenario.md" %}
[deploying-nex-in-a-ha-scenario.md](deploying-nex-in-a-ha-scenario.md)
{% endcontent-ref %}
