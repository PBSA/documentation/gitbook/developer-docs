# Deploying NEX in a HA scenario

## Introduction

HA is the High Availability scenario which means that a IT system, component, or application can able to operate at a high level, continuously, without intervention for an anticipated time. The sole purpose of the High-availability infrastructure is to deliver quality performance, handle various kinds of loads and failure with minimal or zero downtime.

## Requirements

To deploy NEX in Peerplays with High-availability, minimum 4 servers are required,

* 2 Backend Servers for NEX
* 1 Main HA proxy server
* 1 Backup HA proxy server

## Steps to setup HA scenario

It is recommended to use HA proxy to setup a highly available scenario. Click the link below to learn about the step-by-step procedure to configure HA

{% embed url="https://docs.haproxy.org/2.8/configuration.html#2" %}

## Example

The below code uses the SSL passthrough for end to end encryption. The SSL certificates are passed onto both backend servers (server1, server 2).

<pre class="language-http" data-overflow="wrap"><code class="lang-http"><strong>global
</strong>	log /dev/log	local0
	log /dev/log	local1 notice
	chroot /var/lib/haproxy
	stats socket /run/haproxy/admin.sock mode 660 level admin expose-fd listeners
	stats timeout 30s
	user haproxy
	group haproxy
	daemon

	# Default SSL material locations
	ca-base /etc/ssl/certs
	crt-base /etc/ssl/private

	# See: https://ssl-config.mozilla.org/#server=haproxy&#x26;server-version=2.0.3&#x26;config=intermediate
        ssl-default-bind-ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
        ssl-default-bind-ciphersuites TLS_AES_128_GCM_SHA256:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256
        ssl-default-bind-options ssl-min-ver TLSv1.2 no-tls-tickets

defaults
	log	global
	mode	http
	option	dontlognull
        timeout connect 5000
        timeout client  50000
        timeout server  50000
	errorfile 400 /etc/haproxy/errors/400.http
	errorfile 403 /etc/haproxy/errors/403.http
	errorfile 408 /etc/haproxy/errors/408.http
	errorfile 500 /etc/haproxy/errors/500.http
	errorfile 502 /etc/haproxy/errors/502.http
	errorfile 503 /etc/haproxy/errors/503.http
	errorfile 504 /etc/haproxy/errors/504.http

listen stats
    bind *:8080 # Defines the IP and port for the stats page
    mode http
    stats enable
    stats hide-version
    stats realm Haproxy\ Statistics
    stats uri /stats # The URL for the stats page
    stats auth &#x3C;your-username>:&#x3C;your-password> # Use the respective username and password of the node in use


frontend https_front
    bind *:443
    mode tcp
    default_backend https_back

backend https_back
    mode tcp
    option tcp-check
    tcp-check connect port 3000
    server server1 x.x.x.x:443 check fall 1
    server server2 x.x.x.x:443 check backup fall 1


</code></pre>

There are several methods to deploy high availability node application with HA proxy. Below are the links to check the various configurations,

1. [https://distributedleo.medium.com/haproxy-setup-for-nodejs-fe103d69e90e](https://distributedleo.medium.com/haproxy-setup-for-nodejs-fe103d69e90e)
2. [https://gist.github.com/hxyconan/a35072bc64db8cb0a2bbd4b0681c2d2f](https://gist.github.com/hxyconan/a35072bc64db8cb0a2bbd4b0681c2d2f)
