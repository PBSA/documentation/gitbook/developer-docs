# NEX Deployment

## Steps to deploy the NEX for production on chain:

### 1.  Prerequisites

Install the build dependencies on Linux using apt-get command line tool. It is the tool which helps in handling the packages in Linux. The main purpose of the tool is to retrieve the information and packages from an authenticated source for installation, upgrade, and removal of packages with dependencies.&#x20;

{% code overflow="wrap" %}
```
$ apt-get update 

// update the package index files on the system, display information about available package, version.
```
{% endcode %}

```
$ apt-get install build-essential nasm

// Install the build to compile software
```

### 2. Installation

Node v16+ is required and it can be installed using nvm following the below instruction

#### Installing & updating

To install or update nvm run the [install script](https://github.com/nvm-sh/nvm/blob/v0.39.3/install.sh). There are many ways to install, download and run the script manually, or use cURL or Wget command

```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
```

```
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
```

Running either of the above commands download the script and run it. The script clones the nvm repository to `~/.nvm`, and attempts to add the source lines from the snippet below to the correct profile file (`~/.bash_profile`, `~/.zshrc`, `~/.profile`, or `~/.bashrc`).

**Command to load the nvm is given below**

{% code overflow="wrap" %}
```
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
```
{% endcode %}

Click the below link to learn in detail about the nvm installation, troubleshooting, and usage

{% embed url="https://github.com/nvm-sh/nvm#installing-and-updating" %}

### Check whether you are in the application's root directory and then install app dependencies:

#### To install node using nvm

```
nvm install 16 && nvm use 16
```

The script create a nvm repository and it will be used for cloning.

#### Clone the repository created by using the below command:

```
git clone https://gitlab.com/PBSA/NEX.git 
```

If the user creates a node in their own network, then `<branch name>` has to given by the User. By default, the branch name will be the production branch name.

```
git clone https://gitlab.com/PBSA/NEX.git -b <branch name>
```

#### Install npm using the below command

```
npm install
```

### 3. ENV configuration

Create a file name `.env` in the root of the repository:

```
cp .env.example .env
```

In the `.env` file update the required details,

```
# Token symbol
NEXT_PUBLIC_DEFAULT_TOKEN=''

# Token symbol
NEXT_PUBLIC_DEFAULT_QUOTE=''

# Full URL to the blockchain's faucet
NEXT_PUBLIC_FAUCET_URL=''

# Chain ID of the blockchain
NEXT_PUBLIC_DEFAULT_CHAIN_ID=''
```

Example code to fill the `.env` file,&#x20;

{% code overflow="wrap" %}
```
# Used for connecting to the mainnet:
NEXT_PUBLIC_DEFAULT_TOKEN='PPY'
NEXT_PUBLIC_DEFAULT_QUOTE='BTC'
NEXT_PUBLIC_FAUCET_URL='https://faucet.peerplays.download/faucet/api/v1/accounts'
NEXT_PUBLIC_DEFAULT_CHAIN_ID='6b6b5f0ce7a36d323768e534f3edb41c6d6332a541a95725b98e28d140850134'
```
{% endcode %}

{% hint style="info" %}
If the NEXT\_PUBLIC\_DEFAULT\_CHAIN\_ID is not equal to mainnet chain id (6b6b5f0ce7a36d323768e534f3edb41c6d6332a541a95725b98e28d140850134), then the app will use testnet nodes.
{% endhint %}

### 4. Node list

The node list has the collection of nodes and their respective region. If the user has created a node in their own network, then the information has to be updated to the `nodeslist.ts` file.&#x20;

The file is available in the location:  `/src/api/params/nodesList.ts`&#x20;

<details>

<summary>nodesList.ts </summary>

```
import counterpart from "counterpart";

import { Node } from "../../common/types";

import { testnetCheck } from "./networkParams";

export const nodeRegions = [
  // region of the node follows roughly https://en.wikipedia.org/wiki/Subregion#/media/File:United_Nations_geographical_subregions.png
  "Northern Europe",
  "Western Europe",
  "Southern Europe",
  "Eastern Europe",
  "Northern Asia",
  "Western Asia",
  "Southern Asia",
  "Eastern Asia",
  "Central Asia",
  "Southeastern Asia",
  "Australia and New Zealand",
  "Melanesia",
  "Polynesia",
  "Micronesia",
  "Northern Africa",
  "Western Africa",
  "Middle Africa",
  "Eastern Africa",
  "Southern Africa",
  "Northern America",
  "Central America",
  "Caribbean",
  "South America",
];

// node location could be the city name

export const testnetNodes: Node[] = [
  {
    url: "wss://fake.automatic-selection.com",
    location: counterpart.translate("settings.api_closest"),
  },
  {
    url: "wss://devnet.peerplays.download/api",
    location: "",
    region: "Northern America",
    country: "Canada",
    user: {
      name: "Peerplays Witnesses",
      status: "Witness",
    },
  },
];

export const prodNodes: Node[] = [
  {
    url: "wss://fake.automatic-selection.com",
    location: counterpart.translate("settings.api_closest"),
  },
  {
    url: "wss://ca.peerplays.info",
    location: "",
    region: "Northern America",
    country: "Canada",
    user: {
      name: "Peerplays Witnesses",
      status: "Witness",
    },
  },
  {
    url: "wss://de.peerplays.xyz",
    location: "",
    region: "Western Europe",
    country: "Germany",
    user: {
      name: "Peerplays Witnesses",
      status: "Witness",
    },
  },
  {
    url: "wss://pl.peerplays.org",
    location: "",
    region: "Eastern Europe",
    country: "Poland",
    user: {
      name: "Peerplays Witnesses",
      status: "Witness",
    },
  },
];

export const automaticSelection = "wss://fake.automatic-selection.com";

export const defaultNodesList = testnetCheck ? testnetNodes : prodNodes;me code
```

</details>

The User has to add their node and region in the file. An example is shown below,

<pre class="language-tsconfig" data-overflow="wrap"><code class="lang-tsconfig">{
<strong>    url: "&#x3C;Provide the new network URL>",  
</strong><strong>    #Example:    "wss://devnet.peerplays.download/api"
</strong>    location: "",
    region: "Northern America", # Replace with the Region &#x26; Country of New node
    country: "Canada",
    user: {
      name: "Peerplays Witnesses", # Replace the Name and status of New node
      status: "Witness",
    },
  },
</code></pre>

## 5. Manual starting after installation and ENV configuration

#### Development

```
npm run dev
# or
yarn dev
```

Open [ http://localhost:3000 ](http://localhost:3000)with your browser to see the results.

#### Production

Install pm2 globally

```
 npm install pm2 -g
```

Make sure you are in the application's root directory,

#### A. Build production distribution

```
npm run build
```

#### B. Serve the application

```
pm2 start npm --name <must be unique> -- start
```

Now, the application is ready to be used.

### 6. NGINX configuration - Example configuration to create Proxy

```
server {
        listen 80;
        server_name <domain name or serve ip address>;
        root /var/www/html;
        index index.html index.htm;

        location / {
                proxy_pass             http://127.0.0.1:3000;
                proxy_read_timeout     60;
                proxy_connect_timeout  60;
                proxy_redirect         off;

                # Allow the use of websockets
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection 'upgrade';
                proxy_set_header Host $host;
                proxy_cache_bypass $http_upgrade;
        }
}
```

Click the below link to have a glance at the readme.md file to check the steps

[https://gitlab.com/PBSA/NEX/-/blob/main/README.md](https://gitlab.com/PBSA/NEX/-/blob/main/README.md)&#x20;

## Glossary

| Glossary         | Description                                                                                                                                                                                         |
| ---------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| APT              | Advanced Packaging Tool                                                                                                                                                                             |
| Build Essential  | Form of meta packages that are essential to compile software.                                                                                                                                       |
| nvm              | Node version manager for node.js designed to be installed per-user and invoked per-shell.                                                                                                           |
| nasm             | Netwide Assembler, a portable 80x80 assembler                                                                                                                                                       |
| pm2              | PM2 is a node.js process manager that comes with a built-in load balancer. It helps facilitate production deployments and enables you to keep running applications alive indefinitely.              |
| NGINX            | NGINX is open source software for web serving, reverse proxying, caching, load balancing, media streaming, and more. It started out as a web server designed for maximum performance and stability. |



&#x20;
