# NEX - Blockchain API configuration

## Introduction

While deploying NEX with API dependency, there are few assets that has to be enable. This API must be explicitly enabled on the each testnet and its corresponding API nodes, more specifically on the API nodes that the NEX is using inside of the node list.

## Blockchain API

The purpose of Blockchain API is to obtain various kinds of data stored in the blockchain. The data stored in the blockchain such as blocks, transaction, accounts, balance, etc., can be retrieved through the full node's database.

{% hint style="danger" %}
The blockchain API is not aware of the private keys, and cannot sign in for any transactions. All it does is validate and broadcast transactions to the P2P network.
{% endhint %}

## List of nodes&#x20;

The script below list the region of the node and the node location mentioned here is the city name. If NEX is dependent on the mentioned APIs, then the nodes mentioned in the `nodeList.ts` must have the assets API enabled

<details>

<summary>nodesList.ts</summary>

{% code overflow="wrap" %}
```tsconfig
import counterpart from "counterpart";

import { Node } from "../../common/types";

import { testnetCheck } from "./networkParams";

export const nodeRegions = [
  // region of the node follows roughly https://en.wikipedia.org/wiki/Subregion#/media/File:United_Nations_geographical_subregions.png
  "Northern Europe",
  "Western Europe",
  "Southern Europe",
  "Eastern Europe",
  "Northern Asia",
  "Western Asia",
  "Southern Asia",
  "Eastern Asia",
  "Central Asia",
  "Southeastern Asia",
  "Australia and New Zealand",
  "Melanesia",
  "Polynesia",
  "Micronesia",
  "Northern Africa",
  "Western Africa",
  "Middle Africa",
  "Eastern Africa",
  "Southern Africa",
  "Northern America",
  "Central America",
  "Caribbean",
  "South America",
];

// node location could be the city name

export const testnetNodes: Node[] = [
  {
    url: "wss://fake.automatic-selection.com",
    location: counterpart.translate("settings.api_closest"),
  },
  {
    url: "wss://devnet.peerplays.download/api",
    location: "",
    region: "Northern America",
    country: "Canada",
    user: {
      name: "Peerplays Witnesses",
      status: "Witness",
    },
  },
];

export const prodNodes: Node[] = [
  {
    url: "wss://fake.automatic-selection.com",
    location: counterpart.translate("settings.api_closest"),
  },
  {
    url: "wss://ca.peerplays.info",
    location: "",
    region: "Northern America",
    country: "Canada",
    user: {
      name: "Peerplays Witnesses",
      status: "Witness",
    },
  },
  {
    url: "wss://de.peerplays.xyz",
    location: "",
    region: "Western Europe",
    country: "Germany",
    user: {
      name: "Peerplays Witnesses",
      status: "Witness",
    },
  },
  {
    url: "wss://pl.peerplays.org",
    location: "",
    region: "Eastern Europe",
    country: "Poland",
    user: {
      name: "Peerplays Witnesses",
      status: "Witness",
    },
  },
];

export const automaticSelection = "wss://fake.automatic-selection.com";

export const defaultNodesList = testnetCheck ? testnetNodes : prodNodes;me code
```
{% endcode %}

</details>

## How to Enable Asset API?

1. Create a file named: `api-access.json` inside of your `witness_node_data_dir/`
2. Paste the below content in the JSON file

{% code overflow="wrap" %}
```json
{
    "permission_map" :
    [
	[
	    "*",
	    {
		"password_hash_b64" : "*",
		"password_salt_b64" : "*",
		"allowed_apis" : ["database_api", "network_broadcast_api", "history_api", "bookie_api", "affiliate_stats_api", "sidechain_api", "asset_api"]
	    }
	]
    ]
}
```
{% endcode %}

3. Add the following line to your `config.ini`  file in the API node terminal:

```
api-access = /path/to/api-access.json
```

<details>

<summary>config.ini</summary>

\# endpoint for p2p node to listen on

p2p-endpoint = 127.0.0.1:9777

\


\# p2p nodes to connect to on startup (may specify multiple times)

\# seed-node =

\


\# json array of p2p nodes to connect to on startup

seed-nodes = \["51.222.110.110:9777","95.216.90.243:9777","ca.peerplays.info:9777","de.peerplays.xyz:9777","pl.peerplays.org:9777","seed.i9networks.net.br:9777","witness.serverpit.com:9777"]

\


\# pairs of \[block\_num,block\_id] that should be enforced as checkpoints.

\# checkpoint =

\


\# endpoint for websocket rpc to listen on

rpc-endpoint = 127.0.0.1:8090

\


\# endpoint for tls websocket rpc to listen on

\# rpc-tls-endpoint =

\


\# the tls certificate file for this server

\# server-pem =

\


\# password for this certificate

\# server-pem-password =

\


\# file to read genesis state from

\# genesis-json =

\


\# block signing key to use for init witnesses, overrides genesis file

\# dbg-init-key =

\


\# json file specifying api permissions

\# api-access =

\


\# whether to enable tracking of votes of standby witnesses and committee members. set it to true to provide accurate data to api clients, set to false for slightly better performance.

\# enable-standby-votes-tracking =

\


\# space-separated list of plugins to activate

plugins = account\_history accounts\_list affiliate\_stats bookie market\_history witness

\
\


\# ==============================================================================

\# witness plugin options

\# ==============================================================================

\


\# enable block production, even if the chain is stale.

enable-stale-production = false

\


\# percent of witnesses (0-99) that must be participating in order to produce blocks

required-participation = false

\


\# id of witness controlled by this node (e.g. "1.6.5", quotes are required, may specify multiple times)

\# witness-id =

\


\# ids of multiple witnesses controlled by this node (e.g. \["1.6.5", "1.6.6"], quotes are required)

\# witness-ids =

\


\# tuple of \[publickey, wif private key] (may specify multiple times)

private-key = \["ppy6mryajqq8ud7hvnycfnvpjqcvpscn5so8bhthugyqet5gdw5cv","5kqwrpbwdl6phxujxw37fssqz1jiwsst4cqqzdeyxtp79zkvfd3"]

\
\


\# ==============================================================================

\# debug\_witness plugin options

\# ==============================================================================

\


\# tuple of \[publickey, wif private key] (may specify multiple times)

debug-private-key = \["ppy6mryajqq8ud7hvnycfnvpjqcvpscn5so8bhthugyqet5gdw5cv","5kqwrpbwdl6phxujxw37fssqz1jiwsst4cqqzdeyxtp79zkvfd3"]

\
\


\# ==============================================================================

\# account\_history plugin options

\# ==============================================================================

\


\# account id to track history for (may specify multiple times)

\# track-account =

\


\# keep only those operations in memory that are related to account history tracking

partial-operations = 1

\


\# maximum number of operations per account will be kept in memory

max-ops-per-account = 100

\
\


\# ==============================================================================

\# elasticsearch plugin options

\# ==============================================================================

\


\# elastic search database node url(http://localhost:9200/)

\# elasticsearch-node-url =

\


\# number of bulk documents to index on replay(10000)

\# elasticsearch-bulk-replay =

\


\# number of bulk documents to index on a syncronied chain(100)

\# elasticsearch-bulk-sync =

\


\# use visitor to index additional data(slows down the replay(false))

\# elasticsearch-visitor =

\


\# pass basic auth to elasticsearch database('')

\# elasticsearch-basic-auth =

\


\# add a prefix to the index(peerplays-)

\# elasticsearch-index-prefix =

\


\# save operation as object(true)

\# elasticsearch-operation-object =

\


\# start doing es job after block(0)

\# elasticsearch-start-es-after-block =

\


\# save operation as string. needed to serve history api calls(false)

\# elasticsearch-operation-string =

\


\# mode of operation: only\_save(0), only\_query(1), all(2) - default: 0

\# elasticsearch-mode =

\
\


\# ==============================================================================

\# es\_objects plugin options

\# ==============================================================================

\


\# elasticsearch node url(http://localhost:9200/)

\# es-objects-elasticsearch-url =

\


\# basic auth username:password('')

\# es-objects-auth =

\


\# number of bulk documents to index on replay(10000)

\# es-objects-bulk-replay =

\


\# number of bulk documents to index on a synchronized chain(100)

\# es-objects-bulk-sync =

\


\# store proposal objects(true)

\# es-objects-proposals =

\


\# store account objects(true)

\# es-objects-accounts =

\


\# store asset objects(true)

\# es-objects-assets =

\


\# store balances objects(true)

\# es-objects-balances =

\


\# store limit order objects(false)

\# es-objects-limit-orders =

\


\# store feed data(true)

\# es-objects-bitasset =

\


\# store account role objects (true)

\# es-objects-account-role =

\


\# store committee member objects(true)

\# es-objects-committee-member =

\


\# store nft objects (true)

\# es-objects-nft =

\# store son objects (true)

\# es-objects-son =

\# store transaction objects (true)

\# es-objects-transaction =

\# store vesting balance objects (true)

\# es-objects-vesting-balance =

\# store witness objects (true)

\# es-objects-witness =

\# store worker objects (true)

\# es-objects-worker =

\# add a prefix to the index(ppobjects-)

\# es-objects-index-prefix =

\# keep only current state of the objects(true)

\# es-objects-keep-only-current =

\# start doing es job after block(0)

\# es-objects-start-es-after-block =

\# ==============================================================================

\# market\_history plugin options

\# ==============================================================================

\# track market history by grouping orders into buckets of equal size measured in seconds specified as a json array of numbers

bucket-size = \[15,60,300,3600,86400]

\# how far back in time to track history for each bucket size, measured in the number of buckets (default: 1000)

history-per-size = 1000

\# ==============================================================================

\# peerplays\_sidechain plugin options

\# ==============================================================================

\# id of son controlled by this node (e.g. "1.33.5", quotes are required)

\# son-id =

\# ids of multiple sons controlled by this node (e.g. \["1.33.5", "1.33.6"], quotes are required)

\# son-ids =

\# tuple of \[publickey, wif private key] (may specify multiple times)

peerplays-private-key = \["",""]

\# sidechain retry throttling threshold

sidechain-retry-threshold = 150

\# outputs rpc calls to console

debug-rpc-calls = 0

\# bitcoin sidechain handler enabled

bitcoin-sidechain-enabled = 0

\# ip address of bitcoin node

bitcoin-node-ip = 127.0.0.1

\# zmq port of bitcoin node

bitcoin-node-zmq-port = 11111

\# rpc port of bitcoin node

bitcoin-node-rpc-port = 8332

\# bitcoin rpc user

bitcoin-node-rpc-user = 1

\# bitcoin rpc password

bitcoin-node-rpc-password = 1

\# bitcoin wallet

\# bitcoin-wallet =

\# bitcoin wallet password

\# bitcoin-wallet-password =

\# tuple of \[bitcoin public key, bitcoin private key] (may specify multiple times)

bitcoin-private-key = \["",""]

\# hive sidechain handler enabled

\#hive-sidechain-enabled = 0

\# hive node rpc url \[http\[s]://]host\[:port]

hive-node-rpc-url = 127.0.0.1:28090

\# hive node rpc user

\# hive-node-rpc-user =

\# hive node rpc password

\# hive-node-rpc-password =

\# tuple of \[hive public key, hive private key] (may specify multiple times)

hive-private-key = \["tst6llegbaglay28ehrffbvuanfwcfgmqrmw13wbmtexqfe9sckg4","5jnhfzykgaomsfvd4nudq9qmceac43kujbfjuethpvapx1kzq2n"]

\# ==============================================================================

\# snapshot plugin options

\# ==============================================================================

\# block number after which to do a snapshot

\# snapshot-at-block =

\# block time (iso format) after which to do a snapshot

\# snapshot-at-time =

\# pathname of json file where to store the snapshot

\# snapshot-to =

\# ==============================================================================

\# logging options

\# ==============================================================================

\#

\# logging configuration is loaded from logging.ini by default.

\# if logging.ini exists, logging configuration added in this file will be ignored.\


</details>

4. Restart the node to reflect the change.
