# FS-Achievements

## 1. Purpose

The purpose of this document is to explain the functionality expected for the achievement's page in the together application. The main focus is to describe about the achievement earned, user achievements, group achievements, multiple achievement levels.

## 2. Scope

The Functional specification document will describe the requirements, components involved, and basic design to be implemented.

## 3. Component

The proposed configuration, component, and the process flow covered in this document includes:

1. Achievement logo
2. Name of the achievement
3. Image of the achievement
4. An indicator level
5. Achievement description
6. Status bar

## 4. Context

The achievements are earned by users and groups (issues) by performing certain actions a specific number of times. User achievements are earned only by the actions completed by that user, whereas group achievements are earned by the combined actions of all members of the group. Often, achievements have multiple levels in many cases. Users and groups can level up achievements by performing the required action more times. Each subsequent level requires more actions completed than the last.

## 5. Design Wireframe

The below wireframes describes the various components and their designs for the together app.

<figure><img src="../../.gitbook/assets/FS-achievement.jpg" alt="" width="496"><figcaption><p>Achievement section in together app</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/Achieve-logo.JPG" alt=""><figcaption><p>List of achievement's logo</p></figcaption></figure>

## 6. Requirements

The requirement section explain in details about the requirements to develop achievement section at various levels in details.

### A. Achievement Section

An achievement is the result of User's task accomplishment. For every task completion the user will be awarded with the respective badge. The below points should be followed while creating the UI design for achievement section:

A standard **blank achievement image** for achievements which haven’t yet been earned. Example Image shall be similar to the below image:

<figure><img src="https://lh3.googleusercontent.com/_gjpH07OSyFrYiSxFNpUZ9pMKsnvjRAze5I03A0elwnwNAFT0QFDa-WOlIgz8KtYRt8oq04YjTXHJj0ekl9HDBWsAKjD38C0fmBhL3zKuZuN5rtnWa0UimJlC5hTPEkM1V_hVwv5ZDD8YbbP2G2gsEY" alt="" width="188"><figcaption><p>Unearned Achievement Logo</p></figcaption></figure>

The achievement design assets are located in the Figma page given below: [https://www.figma.com/file/KKi8eIqDaHrXMB0t6mL6NQ/Together-%7C-Discovery?type=design\&node-id=1401-38276\&mode=design\&t=rAhpcLSH0LukbVaI-0](https://www.figma.com/file/KKi8eIqDaHrXMB0t6mL6NQ/Together-|-Discovery?type=design\&node-id=1401-38276\&mode=design\&t=rAhpcLSH0LukbVaI-0)

### B. Achievement Levels

There are various levels of achievements based on the User's action. The achievement section consists of four categories namely Image, Name, Description, and Status bar. The below configuration must be followed while creating different achievement categories:

#### 1. Name

The name of the achievement will vary based on the level reached by the user. The names that can be eared by users are Subscriber, Influencer, Professional Writer, Superstar, Supporter, volunteer, etc.,&#x20;

#### 2. Image

The image of the achievement will vary based on the level reached by the user. Each name earned by the user will have the image specific to that and should be updated as the User's levels alter. Example images are listed below:

<figure><img src="https://lh6.googleusercontent.com/M6W5qJJCd3-Od0k_4OaPAUZ5gnW3TfKkafm2K2Fl54xpqAVGpojdaYlcniICyq07zSIrTjy4kIlOe3y2ub5NQVBAecC4RvVV7L_Go4wgPAojn1hzq76NcwztLhwrXQNpnkjTCnK3dBpH2YkjAl_0jK0" alt="" width="188"><figcaption><p>Achievement Level 1 (Name: Better With Two)</p></figcaption></figure>

<figure><img src="https://lh3.googleusercontent.com/vC9HKPTyxEov2wGx0q7N7GhfpP_a9Ja5412nBSFdYF0xGvp-18JuHw3VTcFuZXkIkLiglqi6_6dVadFfj3YMbtBivXGmhIC_aUfCRBncZNc4Upsv-VrOGysPagGVJSCgZII8YuGvL67cpIORGm2qH-U" alt="" width="188"><figcaption><p>Achievement Level 2 (Name: Fist Bump)</p></figcaption></figure>



#### 3. Description

Each achievement earned has a unique description and the details should include the reason of achievement "What it's for?" and "How it's earned?".&#x20;

#### 4. Status Bar

A status bar (with numbers like “17/20”) shall be added below the description to indicate the number of actions required by the user to reach the next level.

The below diagram is an example of an achievement



<figure><img src="../../.gitbook/assets/achievement- desc.JPG" alt="" width="179"><figcaption><p>An Achievement example</p></figcaption></figure>

The achievement names, actions, and levels are located in the spreadsheet below: [https://docs.google.com/spreadsheets/d/1TG8pLoylT-VbF9KRnGijxc3VdoKk3-lTvNXzxK3ux6c/edit#gid=2136638298](https://docs.google.com/spreadsheets/d/1TG8pLoylT-VbF9KRnGijxc3VdoKk3-lTvNXzxK3ux6c/edit#gid=2136638298)

### c. Achievement Indicator

A user shall have any number of achievements based on their actions. So, each achievement must be displayed with the indicator to remind the number of actions required by the user to earn their achievements.

The below image is an example of achievements at various levels:

<figure><img src="../../.gitbook/assets/achieve-level-indicator.JPG" alt=""><figcaption><p>Achievements to be earned</p></figcaption></figure>

\
