# FS-Subscription Plan

## 1. Purpose

The purpose of this document is explain about the subscription plan to be designed for the Toto application. The main focus is to describe the price points, type of subscription, user credits, and the features planned in near future.

## 2. Scope

The Functional specification document will describe the requirements, components involved, and basic design to be implemented.

## 3. Components

The proposed configuration, component, and process flow covered in this document includes:

1. Subscription price points
2. Subscription plan
3. User credits
4. Private group
5. Achievements
6. New ideas for future implementation

## 4. Context

The context of the page must have a proper structure or even template which must provide a consistent User Experience throughout the application. The subscription page will be available under the Profile -> Settings page in the app and provides access to options like subscription plans, buy credits, use credits, and cancel subscription.

## 5. Wireframe Design

The subscription plan shall have the following pages to categorize the various options such as subscribe/unsubscribe, paid features.

### A. Settings

The below representation will help the user to "Subscribe" to the application in order to become a premium member. The list of benefits for the Users will be listed and also shows an option to cancel their subscription.

<figure><img src="../../.gitbook/assets/Setting-subscription.JPG" alt=""><figcaption></figcaption></figure>

### B. Subscription

Two types of users are possible for an application namely, New user and existing user. The below representation will explain the steps for a new user to subscribe and update the plan.

<figure><img src="../../.gitbook/assets/No-subscription.JPG" alt=""><figcaption></figcaption></figure>

This representation describes the steps for an existing user to cancel subscription.

<figure><img src="../../.gitbook/assets/Cancel-subscription.JPG" alt=""><figcaption></figcaption></figure>

## C. Paid Features

The User will be given option to "Update the plan" while selecting the subscription.&#x20;

<figure><img src="../../.gitbook/assets/Payed-feature-subscription.JPG" alt=""><figcaption></figcaption></figure>

If the user selects "Buy Credits", then the list of credits and its benefits will be shown as below.

<figure><img src="../../.gitbook/assets/Payed-feature-buy-credits.JPG" alt=""><figcaption></figcaption></figure>

## 6. Requirements

The below points will describe the requirements specific to the together components.

### A. Subscription

1. The price point for **Subscription** should offer the following **price points** for toto customers:\
   \
   a. $20 / Month\
   b. $200 / Year (that’s three months for free)\
   These price point should be configurable for any future updates in price.



2. There should be a functionality for **automatic recurring** for the subscribers either **Monthly or** **Annually** (based on the User's selection) until the user cancels the subscription from their profile page.



3. The following **benefits should be awarded for the Subscribers**

* (Monthly subscribers) Each month that is paid for, the user shall receive a number of Toto Coins (credits) equivalent to the cost of one letter. (Hence, “One free letter per month”)
* (Yearly subscribers) Each year that is paid for, the user shall receive a number of Toto Coins equivalent to the cost of twelve letters. (Hence, “One free letter per month”)
* Create unlimited private groups (Issues) for free\
  Subscribers don't have to pay any toto coins to create a private issue.\
  Unsubscribed User have to pay some toto coins per private issues created.
* Get a special achievement for becoming a subscriber. (The implementation is in the pipeline and the functionality will be  available soon)

Subscribers are welcome to buy more Toto Coins to send more letters whenever they want to.

### B. Refund

If the user has spent their toto coins for any activities in the application, then the refund process is not possible. If we have the ability to manage the User's toto coins, then there is a possibility to recall the coins and issue them as a pro-rated refund.

### C. Future Ideas to enhance TOTO

The following ideas can be implemented based on the requirements:

* Schedule follow-up letters function.
* Access to the experts library for guides on community building and activism.
* Discounts on Toto Coins.
* Get a special avatar frame the user can display across the app wherever their avatar appears.&#x20;
* Express mailing options.
* Advanced letter tracking with express mailing options.&#x20;
* Priority customer support.&#x20;

### Useful Links:

**Figma Dashboard link:**

&#x20;[https://www.figma.com/file/KKi8eIqDaHrXMB0t6mL6NQ/Together-%7C-Discovery?type=design\&node-id=1636-59353\&mode=design\&t=5K4iHDnGNFHbuVS2-0](https://www.figma.com/file/KKi8eIqDaHrXMB0t6mL6NQ/Together-|-Discovery?type=design\&node-id=1636-59353\&mode=design\&t=5K4iHDnGNFHbuVS2-0)

