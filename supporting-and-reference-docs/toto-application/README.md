# TOTO Application

## What is TOTO?

An AI automated application which helps in writing personalized advocacy letters, mailing physical copies to state representatives, and maintains a record of the letter on the blockchain.

Use the below link to explore TOTO and its supported features:

{% embed url="https://app.usetoto.com/" %}
