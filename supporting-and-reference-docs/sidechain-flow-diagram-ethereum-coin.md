---
description: Deposits and withdraws of Ethereum coins using Peerplays sidechain
---

# Sidechain Flow Diagram (Ethereum coin)

<figure><img src="../.gitbook/assets/eth.png" alt=""><figcaption><p>ETH Deposit and withdraw Flow diagram</p></figcaption></figure>
