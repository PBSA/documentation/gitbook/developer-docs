# API Node

## Introduction

The API node is the node with the open RPC port. They provide a gateway to blockchain function by exposing the API. It is the Application Programming interface to interact with the blockchain. The main purpose of the API node is to provide data and functionality of any desired information.&#x20;

## API Categories

1. **Blockchain API** - To query blockchain data such as accounts, assets, trading history, etc.,\
   It doesn't require private keys to access the information
2. **CLI Wallet API** - The private keys are required to interact with the blockchain with new transaction.

## Features

* The API node runs on the Peerplays core software.
* It keep tracks of the node state.
* The API node is involved in the P2P communication with the nodes that are sharing the block transaction.
* API ports helps the client to view the function of the node.
* The public facing API node needs more RAM as the data retrieval and storage will be on large scale.
* API node is different that the block producer as the API node functionality is only to return requested data about the node for client.
* API node is the public window to access the details.

## Configuration

The public API node can be configured by modifying the **rpc-endpoint** details on the witness node. Below are requirements to configure a API on the witness node,

## 1. Hardware Requirement

The hardware requirements for installing and operating the API node is given below

## A. Peerplays Mainnet

The following detail explain the minimum requirements for running a Peerplays API node on **`Mainnet`**:

| Node Type  | CPU     | Memory :warning: | Storage   | Bandwidth | OS           |
| ---------- | ------- | ---------------- | --------- | --------- | ------------ |
| API (Full) | 1 Cores | 4GB              | 100GB SSD | 1Gbps     | Ubuntu 20.04 |

## B. Peerplays Testnet

The following detail what should be considered the minimum requirements for running a API node on **`Testnet`**:

| **Node Type** | **CPU** | **Memory** :warning: | **Storage** | Bandwidth | OS           |
| ------------- | ------- | -------------------- | ----------- | --------- | ------------ |
| API (Full)    | 4 Cores | 8GB                  | 50GB SSD    | 1Gbps     | Ubuntu 20.04 |

{% hint style="danger" %}
**For all nodes:** The memory requirements shown in the table above are adequate to operate the node. Building and installing the node from source code (as with the manual install) will require more memory. If the system memory is too low, there may be errors during the build and install process. See [Installing vs Operating](https://infra.peerplays.com/the-basics/hardware-requirements#4.2.-installing-vs-operating) for more details. Using Docker or GitLab artifacts for installations don't have this limitation because they use pre-built binaries.
{% endhint %}

The prerequisites to run a RPC is to have a full node or a wallet running and listening to Port 8090, locally.

To make the node listen Port 8090, make the following changes in the `config.ini` file of any  new/existing witness node.

## 2. Update the config.ini file

The value of existing **rpc-endpoint** should be update with the below input and restart the node to reflect the changes.

```
rpc-endpoint = 0.0.0.0:8090
```

Now, the node become a Full node and it will be available to read any API calls to provide the desired output.

The installation guide to install witness node is available in the below location,

{% embed url="https://infra.peerplays.com/witnesses/installation-guides" %}

## 3. Start the node

```
./witness_node
```

## Peerplays API nodes

The list of Peerplays mainnet API node maintained as GitHub gist is mentioned below

```javascript
const endpoints = [
    'wss://ca.peerplays.info/',
    'wss://de.peerplays.xyz/',
    'wss://pl.peerplays.org/',
    'ws://96.46.48.98:18090',
    'wss://peerplaysblockchain.net/mainnet/api',
    'ws://witness.serverpit.com:8090',
    'ws://api.i9networks.net.br:8090',
    'wss://node.mainnet.peerblock.trade'
];
```
