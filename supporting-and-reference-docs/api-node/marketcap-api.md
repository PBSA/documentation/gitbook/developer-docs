# MarketCap API

## Introduction

The purpose of MarketCap API is to provide API endpoints for queries that need responses in bespoke formats required by a particular client or set of clients.  The motivation for this software is primarily to provide an API for market cap aggregator sites, who in general do not specialize their queries for each project but instead require the projects conform to their own request standards. Towards that end, the Marketcap API is a simple and concise REST API that supplies data on max and circulating supply and token rich lists, and could be extended to include other queries.  Responses are structured to conform to the requirements of each market cap aggregator site that is served. This API is not intended for general public use, nor does it offer a comprehensive set of queries.  In particular, it is not suited for use as a block explorer.  Rather, it is for automated polling by a limited number of aggregator sites or statistics recording services, or other special purposes for which a custom API format is needed.

Functionally, the MarketCap API is a wrapper or "facade" that sits in front of the Peerplays core public API nodes, and and simply reformats the responses to fit the needs of the clients served.

## Steps to setup the MarketCap API

### A. Setup

Installation and setup instructions are detailed in the [project's README](https://gitlab.com/PBSA/peerplays-1.0/tools-libs/marketcap-api), but a summary of the steps is as follows:

1. Execute the below CLI in the terminal on the machine on which you will run the service:

```
git clone https://gitlab.com/PBSA/peerplays-1.0/tools-libs/marketcap-api.git
cd marketcap-api
virtualenv env
source env/bin/activate
pip install -r requirements.txt
cd conf
cp -i explorer-conf-sample.yaml explorer-conf.yaml
nano explorer-conf.yaml # Edit configuration
cd ..
```

2. Edit `conf/explorer-conf.yaml` \
   The API needs the URL of a Peerplays core node API with **assets** API enabled.  Note that the assets API is not enabled by default.  For instructions on enabling it, see [_NEX - Blockchain API configuration_](../nex-deployment-and-configuration/nex-blockchain-api-configuration.md#how-to-enable-api-asset).

```yaml
# explorer-conf-sample.yaml
#
# Copy to explorer-conf.yaml and edit.
#

# Where can we find a Peerplay API node with "asset" API open?
peerplays-api-node: wss://testnet.peerplays.download/api

# Where should this API listen?
listen-host: 0.0.0.0
listen-port: 8123

# URL prefix for all endpoints?
# e.g. "/api" or "/api/v1".  Can be empty "" if none.
# Note: should match basePath in swagger.yaml
url-prefix: "/api"
```

### B. Run

The MarketCap API is a simple python3 script. Execute it with the below CLI commands to activate the interface.

```
source env/bin/activate
python3 ./cmc-api.py
```

It is a good idea to run the API in some form of persistent session manager, such as a GNU Screen session, or [PM2](https://pm2.keymetrics.io/).  The latter, if configured correctly, can even enable automated restart on system reboots.

### C. Usage

The API is explained via a Swagger-UI interface, which documents each available API call as well as its parameters and return values. Swagger is an interactive API documentation that also lets you "try out" the API calls.  It will give you a `curl` command that you can run to query the API, and also show you the results of a live query right in the interface.

After running the API, point a browser at http://\[host]:\[port]/api/docs to access the UI.

In the case of the API maintained for Peerplays by the PBSA, the UI can be explored here: [http://marketcap.peerplays.download/api/docs](http://marketcap.peerplays.download/api/docs)

Click the below browser link to access the UI,

{% embed url="http://marketcap.peerplays.download/api/docs#/" %}
The swagger link to check the query
{% endembed %}

## Swagger-UI interface

The below images will showcase the interface for one of the API calls in swagger. The Asset ID is vital to get the output.

<figure><img src="../../.gitbook/assets/supply-swagger.JPG" alt=""><figcaption><p>Drop down option to execute Supply API Call</p></figcaption></figure>

Click on the **Try it out** option to execute the API Call,

<figure><img src="../../.gitbook/assets/Execute-supply.JPG" alt=""><figcaption><p>Click "Execute" to perform API call</p></figcaption></figure>

Click the **Execute** button to run the API call and the output for the respective API call will be listed below. The sample output is provided below,

<figure><img src="../../.gitbook/assets/Supply-output.JPG" alt=""><figcaption></figcaption></figure>

The response for any API call consists of two methods to get the output,

1. Curl command - It uses "**GET**" command as CLI to retrieve the output in the terminal.
2. Requested URL - The URL will be generated for the executed API call. Paste the URL in the browser and click **Enter** to get the plain text output in the browser.

<figure><img src="../../.gitbook/assets/Supply-response.JPG" alt=""><figcaption></figcaption></figure>

The output for CURL and plain text URL is attached below (respectively),

<figure><img src="../../.gitbook/assets/curl-output.JPG" alt=""><figcaption><p>CURL output</p></figcaption></figure>



<figure><img src="../../.gitbook/assets/URL-output.JPG" alt=""><figcaption><p>Plain Text URL output</p></figcaption></figure>

## API Calls

There are three categories of API calls featured in the swagger,

1. Supply
2. Plain-text supply
3. Rich list

## 1. Supply

### 1.1 supply

The command retrieve the complete supply data of an asset using the Asset ID in JSON format. The supply data includes total, maximum, and circulating value of an asset.

**Curl Command**

```json
curl -X 'GET' \
  'http://marketcap.peerplays.download:80/api/supply?asset=<Asset ID>' \
  -H 'accept: application/json'
```

**Plain text URL**

```
http://marketcap.peerplays.download:80/api/supply?asset=<Asset ID>
```

{% tabs %}
{% tab title="Parameters" %}
**Asset ID:** The Asset ID or token symbol of the account&#x20;
{% endtab %}

{% tab title="Return" %}
Return the total, maximum, and circulating value of an asset in JSON format
{% endtab %}

{% tab title="Results" %}
**Response body**

```json
{
  "id": "1.3.0",
  "symbol": "PPY",
  "maximum": "14460519.93031",
  "total": "14460519.93031",
  "circulating": "5901757.32564"
}
```

**Response headers**

{% code overflow="wrap" %}
```
 connection: keep-alive  content-length: 120  content-type: application/json  date: Fri,30 Jun 2023 10:37:03 GMT  server: nginx/1.18.0 (Ubuntu) 
```
{% endcode %}

**Request duration**

```
108 ms
```
{% endtab %}
{% endtabs %}



## 2. Plain-text supply

### 2.1 max\_supply

The max\_supply API call will return the maximum number of coins or token that has ever been created for any asset. Once the maximum supply is reached there cannot be any new coins/token that can be mined, minted or produced. the output is as a fixed point plain text using Asset ID as parameter.

**Curl Command**

```json
curl -X 'GET' \
  'http://marketcap.peerplays.download:80/api/max_supply?asset=<Asset ID>' \
  -H 'accept: application/text'
```

**Plain text URL**

```
http://marketcap.peerplays.download:80/api/max_supply?asset=<Asset ID>
```

{% tabs %}
{% tab title="Parameters" %}
**Asset ID:** The Asset ID or token symbol of the account&#x20;
{% endtab %}

{% tab title="Return" %}
Return the maximum number of coins/token created for the asset.
{% endtab %}

{% tab title="Results" %}
| <p><br><strong>Response body</strong></p><pre><code>14460519.93031
</code></pre><p><strong>Response headers</strong></p><pre data-overflow="wrap"><code>connection: keep-alive  content-length: 14  content-type: text/plain; charset=utf-8  date: Fri,30 Jun 2023 10:25:40 GMT  server: nginx/1.18.0 (Ubuntu) 
</code></pre><p><strong>Request duration</strong></p><pre><code>481 ms
</code></pre> |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
{% endtab %}
{% endtabs %}

### 2.2 total\_supply

The total\_supply API call will return the cumulative supply of coins or tokens of a specific asset that has been created/mined which includes locked/reserved asset. The output is the plain text fixed number using Asset ID as parameter.

**Curl Command**

```
curl -X 'GET' \
  'http://marketcap.peerplays.download:80/api/total_supply?asset=<ASSET ID>' \
  -H 'accept: application/text'
```

**Plain text URL**

```
http://marketcap.peerplays.download:80/api/total_supply?asset=<ASSET ID>
```

{% tabs %}
{% tab title="Parameters" %}
**Asset ID:** The Asset ID or token symbol of the account&#x20;
{% endtab %}

{% tab title="Return" %}
Return the total supply of coins or token of a specific asset.
{% endtab %}

{% tab title="Results" %}
**Response body**

```
14460519.93031
```

**Response headers**

{% code overflow="wrap" %}
```
 connection: keep-alive  content-length: 14  content-type: text/plain; charset=utf-8  date: Fri,30 Jun 2023 10:32:53 GMT  server: nginx/1.18.0 (Ubuntu) 
```
{% endcode %}

**Request duration**

```
248 ms
```
{% endtab %}
{% endtabs %}

### 2.3 circulating\_supply

The circulating\_supply API call returns the total number of coins or token actively available for trade. The output is the plain text fixed number using the asset ID as parameter.

**Curl Command**

```
curl -X 'GET' \
  'http://marketcap.peerplays.download:80/api/circulating_supply?asset=<Asset ID>' \
  -H 'accept: application/text'
```

**Plain text URL**

```
http://marketcap.peerplays.download:80/api/circulating_supply?asset=<Asset ID>
```

{% tabs %}
{% tab title="Parameters" %}
**Asset ID:** The Asset ID or token symbol of the account&#x20;
{% endtab %}

{% tab title="Return" %}
Return the total number of coins or tokens actively available for an asset.
{% endtab %}

{% tab title="Results" %}
**Response body**

```
5901757.32564
```

**Response headers**

{% code overflow="wrap" %}
```
connection: keep-alive  content-length: 13  content-type: text/plain; charset=utf-8  date: Fri,30 Jun 2023 10:36:52 GMT  server: nginx/1.18.0 (Ubuntu) 
```
{% endcode %}

**Request duration**

```
464 ms
```
{% endtab %}
{% endtabs %}

## 3. Rich List

### 3.1 top\_balances

The top\_balances API call will return the top holders of asset for the asset ID with the given number of counts to return the holders.&#x20;

**Curl Command**

{% code overflow="wrap" %}
```json
curl -X 'GET' \
  'http://marketcap.peerplays.download:80/api/top_balances?asset=<Asset ID>&num=<Number>' \
  -H 'accept: application/json'
```
{% endcode %}

**Plain text URL**

```
http://marketcap.peerplays.download:80/api/top_balances?asset=<Asset ID>&num=<Number>
```

{% tabs %}
{% tab title="Parameters" %}
**Asset ID:** The Asset ID or token symbol of the account&#x20;

**Number**: Number of top holders to return for the given asset ID
{% endtab %}

{% tab title="Return" %}
Return the list of top holders of an asset.
{% endtab %}

{% tab title="Results" %}
**Response body**

```json
[
  {
    "name": "q-anon",
    "account_id": "1.2.12784",
    "balance": "177275.29265",
    "symbol": "PPY"
  },
  {
    "name": "livecoin.net",
    "account_id": "1.2.10568",
    "balance": "142345.32445",
    "symbol": "PPY"
  },
  {
    "name": "ppy-holdings",
    "account_id": "1.2.12308",
    "balance": "110003.22036",
    "symbol": "PPY"
  },
  {
    "name": "peerplaysassets1",
    "account_id": "1.2.12312",
    "balance": "103203.02072",
    "symbol": "PPY"
  },
  {
    "name": "rudex-gateway",
    "account_id": "1.2.10951",
    "balance": "100893.17481",
    "symbol": "PPY"
  },
  {
    "name": "williamhill1934",
    "account_id": "1.2.8821",
    "balance": "85302.57246",
    "symbol": "PPY"
  },
  {
    "name": "assetclass92",
    "account_id": "1.2.12313",
    "balance": "85002.48709",
    "symbol": "PPY"
  },
  {
    "name": "asset-block",
    "account_id": "1.2.12310",
    "balance": "84902.48416",
    "symbol": "PPY"
  },
  {
    "name": "azzytota007",
    "account_id": "1.2.9802",
    "balance": "80527.78974",
    "symbol": "PPY"
  },
  {
    "name": "holding-co",
    "account_id": "1.2.12314",
    "balance": "80002.34074",
    "symbol": "PPY"
  },
  {
    "name": "tompool1",
    "account_id": "1.2.11897",
    "balance": "70629.08105",
    "symbol": "PPY"
  },
  {
    "name": "pew-die-pie",
    "account_id": "1.2.8817",
    "balance": "66902.95941",
    "symbol": "PPY"
  },
  {
    "name": "globalholdings1903",
    "account_id": "1.2.12316",
    "balance": "65901.92775",
    "symbol": "PPY"
  },
  {
    "name": "fbingbing81",
    "account_id": "1.2.8813",
    "balance": "65470.41728",
    "symbol": "PPY"
  },
  {
    "name": "boukhyar1",
    "account_id": "1.2.9758",
    "balance": "57286.92662",
    "symbol": "PPY"
  },
  {
    "name": "crypto-boss",
    "account_id": "1.2.10496",
    "balance": "54194.05184",
    "symbol": "PPY"
  },
  {
    "name": "joycho128",
    "account_id": "1.2.8815",
    "balance": "52103.52277",
    "symbol": "PPY"
  },
  {
    "name": "bts-figaro34",
    "account_id": "1.2.6868",
    "balance": "50401.48402",
    "symbol": "PPY"
  },
  {
    "name": "h92r",
    "account_id": "1.2.8814",
    "balance": "47702.77159",
    "symbol": "PPY"
  },
  {
    "name": "idax-deposit",
    "account_id": "1.2.13285",
    "balance": "46261.38130",
    "symbol": "PPY"
  }
]
```

**Response headers**

{% code overflow="wrap" %}
```
 connection: keep-alive  content-length: 1900  content-type: application/json  date: Fri,30 Jun 2023 10:51:43 GMT  server: nginx/1.18.0 (Ubuntu) 
```
{% endcode %}

**Request duration**

```
264 ms
```
{% endtab %}
{% endtabs %}
