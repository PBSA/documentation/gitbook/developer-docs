---
description: General explanation of the feature from the end user perspective.
---

# User stories

## Story collection 1

### _**NFT Artwork Collector**_

As an NFT Artwork Collector, I'd like to know that there is a place where I can view and show off my collected artwork that is independent of whatever site or application I used to purchase my collected artworks.  Perhaps, I purchased the artwork from a small marketplace specializing in a very niche category of artwork, and perhaps this site doesn't provide the best embedding for sharing to social media, or perhaps it doesn't publicly expose a URL for non-logged-in users to view artworks, making sharing difficult.  In this circumstance, an independent explorer providing an objective view of NFT objects and activity on chain would be of use. Thus an NFT Artwork Collector might use TradeHands Explorer for the following reasons:

#### As an NFT Artwork Collector...

<table><thead><tr><th width="109" align="center">Story #</th><th>I want...</th><th>So that...</th></tr></thead><tbody><tr><td align="center">1.1</td><td>to show off collected art</td><td><ol><li>I can show a pride of ownership</li><li>I can encourage secondary market resale</li></ol></td></tr><tr><td align="center">1.2</td><td>to inspect details of art</td><td>the authenticity of the artwork can be verified</td></tr><tr><td align="center"> 1.3  </td><td>to see the artworks</td><td>I can compare and see what I have vs. what I need</td></tr><tr><td align="center">1.4</td><td>to verify the authenticity</td><td>I can confirm what kind of artwork I possess like rarest, souvenir value, etc.,</td></tr><tr><td align="center">1.5</td><td>to view "traits"</td><td>I can distinguish the difference among the artworks.</td></tr><tr><td align="center">1.6</td><td>to have semantic interpretation</td><td>any user can understand the concepts, value of any artwork.   </td></tr><tr><td align="center">1.7</td><td>to link with social media<br>eg: Facebook</td><td>I can easily share my collections with my social followers.</td></tr></tbody></table>

## Story collection2

### _NFT Artist_

As an NFT Artist, I may be using specialized tools to create my NFTs and selling them on specialized or niche marketplaces with a focused audience that provide a particular "view" of the artworks.  To get a sense of how my NFTs look to the "rest of the world," or to get access to activity information and statistics perhaps not tracked by the marketplace I am selling on, an independent explorer providing an objective view of NFT objects and activity on chain could be useful. Thus an NFT Artist might use TradeHands Explorer for the following reasons,

#### As an NFT Artist...

<table><thead><tr><th width="116" align="center">Story #</th><th>I want...</th><th>So that..</th></tr></thead><tbody><tr><td align="center">1.1</td><td>to see all my own artwork as a gallery</td><td>I can display all my artwork as a collection.</td></tr><tr><td align="center">1.2</td><td>to see owner of his art</td><td>I can have the details of who has purchase my arts.</td></tr><tr><td align="center">1.3</td><td>to see the transaction</td><td>I can monitor the selling price of my artworks. </td></tr><tr><td align="center">1.4</td><td>to verify correct formatting of NFTs</td><td>It can be assured that the NFT is aligned with standards.</td></tr></tbody></table>

## Story collection3

### NFT Investor / Trader

As an NFT investor / trader, I may be using a different application than TradeHands Explorer for actual trading or market activity, but still benefit from an Explorer as it allows an objective, neutral view of objects and activity on chain.

#### As an NFT Investor / Trader...

<table><thead><tr><th width="140" align="center">Story #</th><th>I want...</th><th>So that...</th></tr></thead><tbody><tr><td align="center">1.1</td><td>to see recent transaction</td><td>I can have a check on which artworks are being sold.</td></tr><tr><td align="center">1.2</td><td>to see the high value of transaction</td><td>I can monitor which artwork is popular so that I can invest in future.</td></tr><tr><td align="center">1.3</td><td>to see popular artists/artwork</td><td>I can learn in which artwork I can invest. </td></tr><tr><td align="center">1.4</td><td>to view my own collection in a portfolio </td><td>I can asses the value of my collection</td></tr><tr><td align="center">1.5</td><td>to see active buyers</td><td>I can see the artworks that are being purchased and able to invest in future.</td></tr><tr><td align="center">1.6</td><td>to look at the popular categories using filters</td><td>I can make an investment based on the profits/needs.</td></tr><tr><td align="center">1.7</td><td>to see data on the price/bids/asks over time</td><td>I can trade artworks</td></tr></tbody></table>

## Story collection4

### NFT Lottery User

The NFT Lottery system has a separate application that allows users to inspect lotteries, purchase tickets, and enjoy winnings. However, the lottery application is meant to be deployed by lottery hosts.  Thus, there may be a multiplicity of deployments, each showing lotteries relevant only to the deployment owners.  It may not provide a broad view of all lottery activity on chain, or perhaps a complete view of the individual lotteries in detail. An independent explorer allows users to have an objective view into the activities they are participating in. A lottery participant may use TradeHands Explorer for the following reasons,

#### As an NFT Lottery User...

| Story # |                                I want...                               |                     So that...                     |
| :-----: | :--------------------------------------------------------------------: | :------------------------------------------------: |
|   1.1   |                         to identify the winners                        |       I will be aware of who has won lottery.      |
|   1.2   |                alternate ways to see/verify the lottery                |     I can have a check on the winning numbers.     |
|   1.3   |                    to see who else is participating                    |       I can check the number of participants.      |
|   1.4   |                 to check on the end date and conditions                |       I am fully aware of the ticket details.      |
|   1.5   | to see the tickets that has souvenir value with sematic interpretation | I can save the ticket/sell for a profitable price. |

## Story collection5

### Concert Goer

The Concert Goer will have a separate application for purchasing and using NFT-powered concert tickets. However, such an application will likely have a streamlined, simplified user interface that obscures the details of the underlying NFT objects and blockchain activity. More technically-oriented users may gain comfort and confidence in the event tickets and their relevant apps usage by inspecting the objects on chain using an objective, independent explorer. An explorer can allow users to have a broader view of NFT-powered event ticket activity, perhaps revealing events of interest to them that are not highlighted in the ticket app they are using. Thus a Concert Goer might use TradeHands Explorer for the following reasons:

#### As a Concert Goer...

| Story # |                  I want...                  |                                   So that...                                  |
| :-----: | :-----------------------------------------: | :---------------------------------------------------------------------------: |
|   1.1   |   to quickly find the ticket for the event  |                         It can be blocked to purchase.                        |
|   1.2   |      to verify the ticket authenticity      |             the Ticket issuer and event information can be viewed.            |
|   1.3   |        to see all the issuer details        | so that the website, information, details about  other events can be checked. |
|   1.4   | to see the tickets that have Souvenir value |               I can save the ticket/sell for a profitable price.              |

## Story collection6

### Event Manager

The Event Manager may be using expensive licensed event-management software that provides a full-featured but controlled view of the NFT-powered events they are managing.  For verification and validation, an Event Manager may benefit from an independent explorer that provides an objective view of the objects and activity occurring on chain, including their own event ticket NFTs. Thus, the Event Manager might use TradeHands Explore for the following reasons:

#### As an Event Manager...

| Story # |                             I want...                            |                                                  So that...                                                 |
| :-----: | :--------------------------------------------------------------: | :---------------------------------------------------------------------------------------------------------: |
|   1.1   |               to see all tickets created per events              |                              I can be aware of how much tickets are being sold.                             |
|   1.2   |            to see all the events details in wide view            |                         I have all information about the events in a detailed view.                         |
|   1.3   | to see the events that are created by me using a sort and filter |                                 I can find the events hosted by me at ease.                                 |
|   1.4   |                  to see the sold ticket details                  |                           I can monitor the number of people attending the event.                           |
|   1.5   |                 to see the unsold ticket details                 |                             I can find other alternate ways to sell the tickets.                            |
|   1.6   |             to check on the secondary market activity            | I can monitor various platforms through which tickets are being sold and also find the selling/buying cost. |

## Story collection7

### Mobile/Tablet User

The Mobile/Tablet user can be any of the above persona who expects to have a pleasant experience while browsing the TradeHands Explorer.

#### As a Mobile/Tablet User...

| Story# | I want...                                      | As a/an/any...                                                                                                                                                                                                                                                                                                                                                                              |
| :----: | ---------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|   1.1  | to have a simple page layout                   | <ol><li>NFT Collector/investor/Trader to easily navigate for gathering details of any artwork.</li><li>NFT Artist to showcase my artwork, value for buyers.</li><li>Lottery user to check the winners and souvenir value of my ticket at ease.</li><li>Concert goer to purchase tickets.</li><li>Event Manager to check the activity of any events like time, tickets sold, etc.,</li></ol> |
|   1.2  | to have an easy share control (no right click) | Persona, will be easily able to share details of artworks/tickets over social media as required.                                                                                                                                                                                                                                                                                            |



## Story collection8

### Desktop User

The Desktop User can be any of the above Personas who expect the TradeHands Explorer to showcase the artwork in detail. Desktop user has the following advantages,

1. Much larger screen for viewing content
2. Full access to website content
3. Much easier to navigate content
4. Often have the stability and greater bandwidth

#### As a Desktop User...

| Story# | I want...                                                                                           | As a/an/any..                                                                                                                                                                                                                                                                                                            |
| :----: | --------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
|   1.1  | a detailed representation of  artworks which should contain owner, asset value, highest transaction | <ol><li>NFT Collector to showcase my artwork collection that helps buyers to invest</li><li>Desktop User, I can monitor my own artwork collection, owner of my artwork, transaction details in a single large screen layout.</li><li>NFT Investor/Trader, I can showcase my portfolio in a single page layout.</li></ol> |
|   1.2  | to have easy navigation control                                                                     | Persona can check relevant details of any artworks/tickets for selling/buying/monitoring.                                                                                                                                                                                                                                |
